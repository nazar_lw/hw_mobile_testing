package testcase;

import businessobject.LoginationBO;
import businessobject.MainBO;
import dto.MessageDTO;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import userdata.User;

import java.util.Iterator;
import java.util.List;

import static utils.Utils.captureScreenShot;
import static utils.Utils.initializeUserData;

public class LogInSendLetterAndDeleteLetterTest extends BaseTest {

    @DataProvider(parallel = true)
    public Iterator<Object[]> users() {
        List<User> userList = initializeUserData();
        return userList.stream().map(user -> new Object[]{user}).iterator();
    }

    @Test(dataProvider = "users")
    public void testSendAndDeleteLetter(User user) {

        LoginationBO loginationBO = new LoginationBO();
        MainBO mainBO = new MainBO();

        loginationBO.logIn(user);
        mainBO.openCreateLetterForm();
        MessageDTO messageDTO = new MessageDTO();
        mainBO.createAndSendLetter(messageDTO);
        mainBO.openSentLettersList();
        Assert.assertTrue(mainBO.isSentLetterPresent(), " Sent letter is not present ");
        captureScreenShot();
        mainBO.openSentLetter();
        captureScreenShot();
        Assert.assertTrue(mainBO.getTextFromTheSentLetter().contains(messageDTO.getSubject()),
                " Subject of the letter does not match ");
        mainBO.deleteSentLetter();
        captureScreenShot();
    }
}
