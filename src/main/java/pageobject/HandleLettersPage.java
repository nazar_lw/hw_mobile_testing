package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static logger.CustomLogger.logInfo;
import static utils.Utils.isElementDisplayed;
import static wait.CustomWait.waitUntilVisibilityAndGetElement;

public class HandleLettersPage extends AbstractPage {

    @FindBy(xpath = "//*[@id='tltbt']/*[4]/*")
    private WebElement composeButton;

    @FindBy(xpath = "//*[@id='tltbt']/*[2]/*[2]")
    private WebElement menuButton;

    @FindBy(xpath = "//*[@id='mn_']/*/*/*/*[7]")
    private WebElement getAllSentLettersButton;

    @FindBy(xpath = "//div[@class='Ml' and @tabindex='0' and @role='button']")
    private WebElement checkBox;

    public void openCreateLetterForm() {
        logInfo("Opening a new letter form");
        waitUntilVisibilityAndGetElement(composeButton).click();
    }

    public void openLettersMenu() {
        logInfo("Opening the page with sent letters");
        waitUntilVisibilityAndGetElement(menuButton).click();
    }

    public void getAllSentLettersList() {
        logInfo("Opening the page with sent letters");
        waitUntilVisibilityAndGetElement(getAllSentLettersButton).click();
    }

    public boolean isSentLetterPresent() {
        return isElementDisplayed(waitUntilVisibilityAndGetElement(checkBox));
    }

    public void openTheSentLetter() {
        logInfo("Opening the letter");
        checkBox.click();
    }
}
