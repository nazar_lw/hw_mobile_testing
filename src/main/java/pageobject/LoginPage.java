package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static logger.CustomLogger.logInfo;
import static wait.CustomWait.waitUntilVisibilityAndGetElement;

public class LoginPage extends AbstractPage {

    @FindBy(xpath = "//*[@id='identifierId']")
    private WebElement emailInput;

    @FindBy(id = "identifierNext")
    private WebElement proceedWithEmailButton;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(id = "passwordNext")
    private WebElement proceedWithPasswordButton;

    public void typeEmailAndSubmit(String email) {
        logInfo("Submitting email");
        waitUntilVisibilityAndGetElement(emailInput).sendKeys(email);
        waitUntilVisibilityAndGetElement(proceedWithEmailButton).click();
    }

    public void typePasswordAndSubmit(String password) {
        logInfo("Submitting password and proceeding to HomePage");
        waitUntilVisibilityAndGetElement(passwordInput).sendKeys(password);
        waitUntilVisibilityAndGetElement(proceedWithPasswordButton).click();
    }
}
