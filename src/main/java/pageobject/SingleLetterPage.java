package pageobject;

import dto.MessageDTO;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static logger.CustomLogger.logInfo;
import static wait.CustomWait.waitUntilVisibilityAndGetElement;

public class SingleLetterPage extends AbstractPage {

    @FindBy(xpath = "//*[@id='composeto']")
    private WebElement recipientInput;

    @FindBy(id = "cmcsubj")
    private WebElement subjectInput;

    @FindBy(id = "cmcbody")
    private WebElement mainTextArea;

    @FindBy(xpath = "//*[@id='cvtbt']/*[4]/*")
    private WebElement sendEmailButton;

    public void fillLetter(MessageDTO messageDTO) {
        logInfo("Filling fields of the letter");
        waitUntilVisibilityAndGetElement(recipientInput).sendKeys(messageDTO.getRecipientEmail());
        waitUntilVisibilityAndGetElement(subjectInput).sendKeys(messageDTO.getSubject());
        waitUntilVisibilityAndGetElement(mainTextArea).sendKeys(messageDTO.getContent());
    }

    public void sendLetter() {
        logInfo("Sending the letter");
        waitUntilVisibilityAndGetElement(sendEmailButton).click();
        try {
            logInfo("Letter has been sent, waiting for appearance of the letter in the list");
            Thread.sleep(7500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
