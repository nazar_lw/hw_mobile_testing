package pageobject;

import driver.DriverManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.PageFactory;

import static logger.CustomLogger.logInfo;

public class AbstractPage {

    AbstractPage() {
        logInfo("Initialize PageFactory in: " + this.getClass().getName());
        PageFactory.initElements(DriverManager.getDriver(), this);
    }
}
