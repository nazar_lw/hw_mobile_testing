package pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static logger.CustomLogger.logInfo;
import static logger.CustomLogger.logWarn;
import static wait.CustomWait.waitUntilVisibilityAndGetElement;

public class SentLettersPage extends AbstractPage {

    @FindBy(xpath = "//*[@id='cv_']/*/*/*[1]")
    private WebElement textHolder;

    @FindBy(xpath = "//*[@id='cv__cntbt']/*[4]/*[2]")
    private WebElement deleteButton;

    public String getTextFromElement() {
        logInfo("Getting the text from letter");
        return waitUntilVisibilityAndGetElement(textHolder).getText();
    }

    public void deleteSentLetter() {
        logInfo("Deleting the letter");
        waitUntilVisibilityAndGetElement(deleteButton).click();
    }
}
