package businessobject;

import dto.MessageDTO;
import pageobject.HandleLettersPage;
import pageobject.SentLettersPage;
import pageobject.SingleLetterPage;

public class MainBO {

    private HandleLettersPage handleLettersPage;
    private SingleLetterPage singleLetterPage;
    private SentLettersPage sentLettersPage;

    public MainBO() {
        handleLettersPage = new HandleLettersPage();
        singleLetterPage = new SingleLetterPage();
        sentLettersPage = new SentLettersPage();
    }

    public void openCreateLetterForm() {
        handleLettersPage.openCreateLetterForm();
    }

    public void createAndSendLetter(MessageDTO messageDTO) {
        singleLetterPage.fillLetter(messageDTO);
        singleLetterPage.sendLetter();
    }

    public void openSentLettersList() {
        handleLettersPage.openLettersMenu();
        handleLettersPage.getAllSentLettersList();
    }

    public boolean isSentLetterPresent() {
        return handleLettersPage.isSentLetterPresent();
    }

    public void openSentLetter() {
        handleLettersPage.openTheSentLetter();
    }

    public String getTextFromTheSentLetter() {
        return sentLettersPage.getTextFromElement();
    }

    public void deleteSentLetter() {
        sentLettersPage.deleteSentLetter();
    }
}
