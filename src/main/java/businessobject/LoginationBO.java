package businessobject;

import pageobject.LoginPage;
import userdata.User;

public class LoginationBO {

    private LoginPage loginPage;

    public LoginationBO() {
        loginPage = new LoginPage();
    }

    public void logIn(User user) {
        loginPage.typeEmailAndSubmit(user.getEmail());
        loginPage.typePasswordAndSubmit(user.getPassword());
    }
}
