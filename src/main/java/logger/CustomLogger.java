package logger;

import com.aventstack.extentreports.Status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static reporterconfigs.KlovConfigs.extentTest;


public class CustomLogger {

    private static final Logger logger = LogManager.getLogger(CustomLogger.class);

    public static void logInfo(String log) {
        logger.info(log);
        extentTest.log(Status.INFO, log);
    }

    public static void logDebug(String log) {
        logger.debug(log);
        extentTest.log(Status.DEBUG, log);
    }

    public static void logError(String log) {
        logger.error(log);
        extentTest.log(Status.ERROR, log);
    }

    public static void logWarn(String log) {
        logger.warn(log);
        extentTest.log(Status.WARNING, log);
    }
}