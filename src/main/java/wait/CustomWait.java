package wait;

import driver.DriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.Objects;

import static logger.CustomLogger.logDebug;
import static utils.Utils.getLocatorFromElement;

public class CustomWait {

    private static FluentWait<WebDriver> getNewFluentWait() {
        return new FluentWait<>(DriverManager.getDriver())
                .ignoring(ElementClickInterceptedException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(NoSuchElementException.class)
                .ignoring(TimeoutException.class);
    }

    public static WebElement waitUntilVisibilityAndGetElement(WebElement element) {
        logDebug("Waiting for the WebElement: ( " + getLocatorFromElement(element) + " )");
        return getNewFluentWait().until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitUntilDocumentReadyState() {
        getNewFluentWait().until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) Objects.requireNonNull(wd))
                        .executeScript("return document.readyState")
                        .equals("complete"));
    }
}
