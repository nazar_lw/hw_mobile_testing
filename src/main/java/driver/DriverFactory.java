package driver;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static logger.CustomLogger.logError;
import static logger.CustomLogger.logInfo;
import static utils.PropertyFileHandler.*;

class DriverFactory {

    WebDriver buildDriver() {

        logInfo("Starting AppiumDriver ... ");
        AppiumDriver<MobileElement> driver = null;
        DesiredCapabilities cap = new DesiredCapabilities();
        URL url = null;

        cap.setCapability(MobileCapabilityType.DEVICE_NAME, MOBILE_DEVICE_NAME);
        cap.setCapability(MobileCapabilityType.UDID, MOBILE_DEVICE_UDID);
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, MOBILE_PLATFORM_NAME);
        cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, MOBILE_PLATFORM_VERSION);
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, COMMAND_TIMEOUT);
        cap.setCapability(MobileCapabilityType.BROWSER_NAME, MOBILE_BROWSER_NAME);

        cap.setCapability("chromedriverExecutable", System.getProperty("user.dir") + File.separator + CHROME_DRIVER_PATH);
        //Cannot call non W3C standard command while in W3C mode
        cap.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));

        try {
             url = new URL(APPIUM_SERVER_ENDPOINT);
        } catch (MalformedURLException e) {
            logError(e.getMessage());
        }
        driver = new AppiumDriver<MobileElement>(Objects.requireNonNull(url), cap);
        driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIME_VALUE, TimeUnit.SECONDS);

        return driver;
    }
}