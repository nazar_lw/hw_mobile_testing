package driver;

import org.openqa.selenium.WebDriver;

import static logger.CustomLogger.logInfo;
import static logger.CustomLogger.logWarn;

public class DriverManager {

    private static ThreadLocal<WebDriver> driverPool = new ThreadLocal<>();

    private DriverManager() {
    }

    public static WebDriver getDriver() {
        if (driverPool.get() == null) {
            driverPool.set(new DriverFactory().buildDriver());
        }
        return driverPool.get();
    }

    public static void quitDriver() {
        if (driverPool.get() != null) {
            logInfo("Driver quit");
            driverPool.get().quit();
            driverPool.set(null);
        }
    }
}
