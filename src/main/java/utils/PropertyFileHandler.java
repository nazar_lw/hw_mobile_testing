package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static logger.CustomLogger.logInfo;

public class PropertyFileHandler {
    public static final String CHROME_DRIVER_PATH;
    public static final String MAIN_URL;
    public static final String RECIPIENT_EMAIL;
    public static final String SUBJECT;
    public static final String CONTENT;
    public static final String MOBILE_DEVICE_NAME;
    public static final String MOBILE_DEVICE_UDID;
    public static final String MOBILE_PLATFORM_NAME;
    public static final String MOBILE_PLATFORM_VERSION;
    public static final String MOBILE_BROWSER_NAME;
    public static final int COMMAND_TIMEOUT;
    public static final int IMPLICITLY_WAIT_TIME_VALUE;
    public static final String APPIUM_SERVER_ENDPOINT;

    static {
        Properties prop = new Properties();
        try (InputStream input =
                     new FileInputStream("src\\main\\resources\\config.properties")) {
            prop.load(input);
            logInfo("Constants are extracted from the property file");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        CHROME_DRIVER_PATH = prop.getProperty("chromedriverPath");
        MAIN_URL = prop.getProperty("main_url");
        RECIPIENT_EMAIL = prop.getProperty("recipient_email");
        SUBJECT = prop.getProperty("subject");
        CONTENT = prop.getProperty("content");
        MOBILE_DEVICE_NAME = prop.getProperty("device_name");
        MOBILE_DEVICE_UDID = prop.getProperty("device_udid");
        MOBILE_PLATFORM_NAME = prop.getProperty("platform_name");
        MOBILE_PLATFORM_VERSION = prop.getProperty("platform_version");
        MOBILE_BROWSER_NAME = prop.getProperty("browser_name");
        COMMAND_TIMEOUT = Integer.parseInt(prop.getProperty("command_timeout"));
        IMPLICITLY_WAIT_TIME_VALUE = Integer.parseInt(prop.getProperty("implicit_wait"));
        APPIUM_SERVER_ENDPOINT = prop.getProperty("appium_endpoint");
    }
}
