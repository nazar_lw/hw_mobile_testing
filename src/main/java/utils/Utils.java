package utils;

import com.aventstack.extentreports.MediaEntityBuilder;
import driver.DriverManager;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.*;
import userdata.User;
import userdata.UserDAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static logger.CustomLogger.*;
import static reporterconfigs.KlovConfigs.extentTest;
import static wait.CustomWait.waitUntilDocumentReadyState;

public class Utils {

    public static void goToPageURL(final String url) {
        logInfo("Going to URL: " + url);
        DriverManager.getDriver().get(url);
    }

    public static String getLocatorFromElement(WebElement element) {
        try {
            return Objects.requireNonNull(element)
                    .toString()
                    .split("->")[1]
                    .replaceFirst("(?s)(.*)]", "$1" + "");
        } catch (ArrayIndexOutOfBoundsException e) {
            logError(String.valueOf(e.getCause()));
            return "parsing of locator from element failed !!";
        }
    }

    public static List<User> initializeUserData() {
        List<User> userList = new ArrayList<>();
        try {
            logWarn("Extracting users data ");
            userList = new UserDAO().getAll();
        } catch (IOException e) {
            logError(e.getMessage());
        }
        return userList;
    }

    public static boolean isElementDisplayed(WebElement element) {
        try {
            logInfo("Checking if element: ( " + getLocatorFromElement(element) + " ) is displayed");
            return element.isDisplayed();
        } catch (NoSuchElementException | TimeoutException e) {
            return false;
        }
    }

    public static void captureScreenShot() {
        logInfo("Waiting for the ready state of document to capture the screenshot.. ");
        waitUntilDocumentReadyState();
        byte[] array = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
        String base64string = new String(Base64.encodeBase64(array));
        try {
            extentTest.info("Capturing screenshot on event",
                    MediaEntityBuilder.createScreenCaptureFromBase64String(base64string).build());
        } catch (IOException e) {
            logError(e.getMessage());
        }
    }
}